﻿
# Preparando o Ambiente

É possível executar o projeto usando o docker-compose (com postgres) e sem o o docker-compose (com sqlite e alterando a configuração do django).

## Instalando o docker

	$ sudo apt-get install docker-ce docker-ce-cli containerd.io
	$ docker --version
		# Docker version 19.03.4, build ...


## Instalando o docker-compose

	$ sudo pip install docker-compose
	$ docker-compose --version
		# docker-compose version 1.24.1, build ...


## Instalando o pipenv
Caso for executar o projeto sem usar o docker-compose. No docker-compose, já é feita a instalação do pipenv no container.

	$ sudo apt-get install pipenv
	$ pipenv --version
		# pipenv, version 11.9.0


## Instalando Git

	$ sudo apt-get install git
	$ git --version
		# git version 2.20.1


## Copiando o projeto

	cd ~/
	git clone https://gitlab.com/tomazcunha/tomllapi_environment.git


## Executando o projeto

	$ cd tomllapi_environment
	$ ls
		# docker-compose.yml  nginx  production.yml  web

Criando e iniciando os containers:

	$ docker-compose up -d --build


## Parando os containers

Mantenha o containers ativos para executar os próximos passos. Caso seja necessário parar o docker-compose, utilize:

	$ docker-compose down"

Para reiniciando é só executar novamente:

	$ docker-compose up -d --build


Já deve ser possível acessar o projeto pela url. Obs.: Mantive as migrações.

	http://127.0.0.1:8000/customers/
	http://127.0.0.1:8000/customers.json

	http://127.0.0.1:8000/products/
	http://127.0.0.1:8000/products.json

	http://127.0.0.1:8000/users/
	http://127.0.0.1:8000/users.json


## Fazendo as migrações

Mantive as migrações, então não é preciso executar estes comandos. Entretanto, caso seja necessário refazer as migrações, execute:

	$ docker-compose run web python manage.py makemigrations api
	$ docker-compose run web python manage.py migrate --noinput


## Criando o Super Usuário
	$ docker-compose run web python manage.py createsuperuser --email admin@example.com --username admin

	# Informar senha de oito dígitos.
	# Confirmar senha.
	# exemplo: adminadmin


## Instalando httpie

	$ sudo pip install httpie
	$ http --version
		# 1.0.3


## Manipulando registros de Cliente

Retornando Customers sem autenticação:

	$ http http://127.0.0.1:8000/customers.json

		{
		    "count": 1,
		    "next": null,
		    "previous": null,
		    "results": [
		        {
		            "email": "maria@maria.com.br",
		            "id": 1,
		            "name": "Maria",
		            "products": [
		                1
		            ],
		            "user": "admin"
		        }
		    ]
		}


Retornando Customers com autenticação:

	$ http -a admin:adminadmin http://127.0.0.1:8000/customers.json


Incluindo novo registro:

	$ http -a admin:adminadmin --form POST http://127.0.0.1:8000/customers/ name="Pedro" email="pedro@pedro.com"

		{
		    "email": "pedro@pedro.com",
		    "id": 2,
		    "name": "Pedro",
		    "products": [],
		    "user": "admin"
		}

Não permite gravar o registro com o mesmo email:

	$ http -a admin:adminadmin --form POST http://127.0.0.1:8000/customers/ name="Pedro Augusto" email="pedro@pedro.com"

		HTTP/1.1 400 Bad Request
		Allow: GET, POST, HEAD, OPTIONS
		Content-Length: 54
		Content-Type: application/json
		Date: Mon, 25 Nov 2019 02:47:07 GMT
		Server: WSGIServer/0.2 CPython/3.7.5
		Vary: Accept, Cookie
		X-Frame-Options: SAMEORIGIN

		{
		    "email": [
		        "Customer with this email already exists."
		    ]
		}


Não permite excluir registro sem autenticação:

	$ http --form DELETE http://127.0.0.1:8000/customers/2.json

		{
		    "detail": "Authentication credentials were not provided."
		}


Excluindo registro:

	$ http -a admin:adminadmin --form DELETE http://127.0.0.1:8000/customers/2.json

		HTTP/1.1 204 No Content
		Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS
		Content-Length: 0
		Date: Mon, 25 Nov 2019 03:19:17 GMT
		Server: WSGIServer/0.2 CPython/3.7.5
		Vary: Accept, Cookie
		X-Frame-Options: SAMEORIGIN


Atualizando registro:

	$ http -a admin:adminadmin --form PUT http://127.0.0.1:8000/customers/1.json email="maria_silva@maria.com.br"

		{
		    "email": "maria_silva@maria.com.br",
		    "id": 1,
		    "name": "Maria",
		    "products": [
		        1
		    ],
		    "user": "admin"
		}


## Manipulando registros de Produtos Favoritos

Listando os produtos favoritos:

	$ http http://127.0.0.1:8000/products.json

		{
		    "count": 1,
		    "next": null,
		    "previous": null,
		    "results": [
		        {
		            "customer": 1,
		            "id": 1,
		            "product_fav_id": "4bd442b1-4a7d-2475-be97-a7b22a08a024"
		        }
		    ]
		}


Incluindo novo produto favorito:

	$ http -a admin:adminadmin --json POST http://127.0.0.1:8000/products/ customer_id=1 product_fav_id=958ec015-cfcf-258d-c6df-1721de0ab6ea

		{
		    "customer": 1,
		    "id": 2,
		    "product_fav_id": "958ec015-cfcf-258d-c6df-1721de0ab6ea"
		}


Não é possível incluir produto favorito sem um código de produto válido:

	$ http -a tomaz:tomazadmin --json POST http://127.0.0.1:8000/products/ customer_id=4 product_fav_id=2b505fab

		HTTP/1.1 404 Not Found
		Allow: GET, POST, HEAD, OPTIONS
		Content-Length: 19
		Content-Type: application/json
		Date: Mon, 25 Nov 2019 04:56:14 GMT
		Server: WSGIServer/0.2 CPython/3.7.5
		Vary: Accept, Cookie
		X-Frame-Options: SAMEORIGIN

		"Product not found"



Incluindo favoritos com outro Usuário:

	$ http -a tomaz:tomazadmin --form POST http://127.0.0.1:8000/customers/ name="tomaz" email="tomaz@tomaz.com"

		{
        "email": "tomaz@tomaz.com",
        "id": 4,
        "name": "tomaz",
        "products": [],
        "user": "tomaz"
    }


	$ http -a tomaz:tomazadmin --json POST http://127.0.0.1:8000/products/ customer_id=4 product_fav_id=77be5ad3-fa87-d8a0-9433-5dbcc3152fac

		{
		    "customer": 4,
		    "id": 4,
		    "product_fav_id": "77be5ad3-fa87-d8a0-9433-5dbcc3152fac"
		}


	$ http http://127.0.0.1:8000/customers.json

		{
		    "count": 2,
		    "next": null,
		    "previous": null,
		    "results": [
		        {
		            "email": "maria_silva@maria.com.br",
		            "id": 1,
		            "name": "Maria",
		            "products": [
		                3,
		                2,
		                1
		            ],
		            "user": "admin"
		        },
		        {
		            "email": "tomaz@tomaz.com",
		            "id": 4,
		            "name": "tomaz",
		            "products": [
		                4
		            ],
		            "user": "tomaz"
		        }
		    ]
		}


	$ http http://127.0.0.1:8000/products.json

		{
		    "count": 4,
		    "next": null,
		    "previous": null,
		    "results": [
		        {
		            "customer": 1,
		            "id": 1,
		            "product_fav_id": "4bd442b1-4a7d-2475-be97-a7b22a08a024"
		        },
		        {
		            "customer": 1,
		            "id": 2,
		            "product_fav_id": "958ec015-cfcf-258d-c6df-1721de0ab6ea"
		        },
		        {
		            "customer": 1,
		            "id": 3,
		            "product_fav_id": "6a512e6c-6627-d286-5d18-583558359ab6"
		        },
		        {
		            "customer": 4,
		            "id": 4,
		            "product_fav_id": "77be5ad3-fa87-d8a0-9433-5dbcc3152fac"
		        }
		    ]
		}



Removendo registro de favorito

	$ http -a admin:adminadmin --form DELETE http://127.0.0.1:8000/products/3.json

		HTTP/1.1 204 No Content
		Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS
		Content-Length: 0
		Date: Mon, 25 Nov 2019 04:30:36 GMT
		Server: WSGIServer/0.2 CPython/3.7.5
		Vary: Accept, Cookie
		X-Frame-Options: SAMEORIGIN


## Executando o projeto sem usar o docker-compose

Nesse caso não vamos usar o postgresql como está configurado no docker-compese. Vamos manter o SQLite.

No terminal, entre na pasta do projeto:

	$ cd ~/tomllapi_environment/web/


Em "web/tomllapi/settings.py" descomente esta configuração para ficar assim:

	DATABASES = {
	    'default': {
	        'ENGINE': 'django.db.backends.sqlite3',
	        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
	    }
	}

E comente esta que não vamos usar:

	# DATABASES = {
	#     'default': {
	#         'ENGINE': 'django.db.backends.postgresql',
	#         'NAME': 'postgres',
	#         'USER': 'postgres',
	#         'HOST': 'postgres',   # set in docker-compose.yml
	#         'PORT': 5432    # default postgres port
	#     }
	# }


Estando dentro de "~/tomllapi_environment/web/" execute "pipenv install" para criar uma virtualização e instalar as dependências do projeto:

	$ pipenv install

		Creating a virtualenv for this project…
		Using /usr/bin/python3.7m (3.7.3) to create virtualenv…
		⠋Running virtualenv with interpreter /usr/bin/python3.7m
		Using base prefix '/usr'
		New python executable in /home/tomaz19/.local/share/virtualenvs/web-UmTKMNg_/bin/python3.7m
		Also creating executable in /home/tomaz19/.local/share/virtualenvs/web-UmTKMNg_/bin/python
		Installing setuptools, pip, wheel...
		done.

		Virtualenv location: /home/tomaz19/.local/share/virtualenvs/web-UmTKMNg_
		Installing dependencies from Pipfile.lock (d76e0b)…
		  🐍   ▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉ 26/26 — 00:00:15
		To activate this project's virtualenv, run the following:
		 $ pipenv shell


Para mostras quais foram estas dependências, pode executar:

	$ pipenv graph

		Django==2.2.7
		  - pytz [required: Any, installed: 2019.3]
		  - sqlparse [required: Any, installed: 0.3.0]
		djangorestframework==3.10.3
		httpie==1.0.3
		  - Pygments [required: >=2.3.1, installed: 2.4.2]
		  - requests [required: >=2.21.0, installed: 2.22.0]
		    - certifi [required: >=2017.4.17, installed: 2019.9.11]
		    - chardet [required: >=3.0.2,<3.1.0, installed: 3.0.4]
		    - idna [required: <2.9,>=2.5, installed: 2.8]
		    - urllib3 [required: >=1.21.1,!=1.25.1,<1.26,!=1.25.0, installed: 1.25.7]
		ipdb==0.12.2
		  - ipython [required: >=5.1.0, installed: 7.9.0]
		    - backcall [required: Any, installed: 0.1.0]
		    - decorator [required: Any, installed: 4.4.1]
		    - jedi [required: >=0.10, installed: 0.15.1]
		      - parso [required: >=0.5.0, installed: 0.5.1]
		    - pexpect [required: Any, installed: 4.7.0]
		      - ptyprocess [required: >=0.5, installed: 0.6.0]
		    - pickleshare [required: Any, installed: 0.7.5]
		    - prompt-toolkit [required: >=2.0.0,<2.1.0, installed: 2.0.10]
		      - six [required: >=1.9.0, installed: 1.13.0]
		      - wcwidth [required: Any, installed: 0.1.7]
		    - pygments [required: Any, installed: 2.4.2]
		    - setuptools [required: >=18.5, installed: 42.0.0]
		    - traitlets [required: >=4.2, installed: 4.3.3]
		      - decorator [required: Any, installed: 4.4.1]
		      - ipython-genutils [required: Any, installed: 0.2.0]
		      - six [required: Any, installed: 1.13.0]
		  - setuptools [required: Any, installed: 42.0.0]
		psycopg2-binary==2.8.4


Ativando a virtualização:

	$ pipenv shell

Vai aparecer algo do tipo (web-UmTKMNg_) no início da linha de comando, confirmando que está ativado.

Excluindo o banco e as migrações:

	$ rm -f db.sqlite3
	$ rm -r api/migrations


Criando e executando as migrações:

	$ python manage.py makemigrations api

		Migrations for 'api':
	  api/migrations/0001_initial.py
	    - Create model Customer
	    - Create model Product


	$ python manage.py migrate

		Operations to perform:
		  Apply all migrations: admin, api, auth, contenttypes, sessions
		Running migrations:
		  Applying contenttypes.0001_initial... OK
		  Applying auth.0001_initial... OK
		  Applying admin.0001_initial... OK
		  Applying admin.0002_logentry_remove_auto_add... OK
		  Applying admin.0003_logentry_add_action_flag_choices... OK
		  Applying api.0001_initial... OK
		  Applying contenttypes.0002_remove_content_type_name... OK
		  Applying auth.0002_alter_permission_name_max_length... OK
		  Applying auth.0003_alter_user_email_max_length... OK
		  Applying auth.0004_alter_user_username_opts... OK
		  Applying auth.0005_alter_user_last_login_null... OK
		  Applying auth.0006_require_contenttypes_0002... OK
		  Applying auth.0007_alter_validators_add_error_messages... OK
		  Applying auth.0008_alter_user_username_max_length... OK
		  Applying auth.0009_alter_user_last_name_max_length... OK
		  Applying auth.0010_alter_group_name_max_length... OK
		  Applying auth.0011_update_proxy_permissions... OK
		  Applying sessions.0001_initial... OK


Criando o Super Usuário:

	$ python manage.py createsuperuser --email admin@example.com --username admin
		# Password: adminadmin
		# Password (again): adminadmin


Executando o projeto:

	$ python manage.py runserver


Abra outro terminal e teste o comando:

	$ http -a admin:adminadmin --form POST http://127.0.0.1:8000/customers/ name="Pedro" email="pp@pppp.com"

		HTTP/1.1 201 Created
		Allow: GET, POST, HEAD, OPTIONS
		Content-Length: 74
		Content-Type: application/json
		Date: Mon, 25 Nov 2019 05:55:18 GMT
		Server: WSGIServer/0.2 CPython/3.7.3
		Vary: Accept, Cookie
		X-Frame-Options: SAMEORIGIN

		{
		    "email": "pp@pppp.com",
		    "id": 1,
		    "name": "Pedro",
		    "products": [],
		    "user": "admin"
		}


Tudo ok.
