from django.test import TestCase
from api.services.product_service import ProductService

class TestProductApi(TestCase):
    producService = ProductService()

    def test_if_product_exists(self):
        code, body = self.producService.get_products_by_id("1bf0f365-fbdd-4e21-9786-da459d78dd1f")
        # print(code)
        # print(body)
        self.assertEqual(code, 200)

    def test_if_product_not_exists(self):
        code, body = self.producService.get_products_by_id("1bf0f365-fbdd-4e21-9786-da459d78djjjj")
        # print(code)
        # print(body)
        self.assertEqual(code, 404) 
