from django.db import models
from django.contrib.auth.models import User

class Customer(models.Model):
    name = models.CharField(blank=True, max_length=100)
    email = models.EmailField(unique=True)
    user = models.ForeignKey('auth.User', related_name='customers', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Customer'
        verbose_name_plural = 'Customers'

    def __str__(self):
        return str(self.name)


class Product(models.Model):
    customer = models.ForeignKey('Customer', related_name='products', on_delete=models.CASCADE)
    product_fav_id = models.CharField(unique=True, max_length=150)

    class Meta:
        verbose_name = 'Product'
        verbose_name_plural = 'Products'

    def __str__(self):
        return str(self.id)
