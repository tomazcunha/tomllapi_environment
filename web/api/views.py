from api.models import Customer, Product
from api.serializers import CustomerSerializer
from api.serializers import ProductSerializer
from api.serializers import UserSerializer
from api.permissions import IsOwnerOrReadOnly
from django.contrib.auth.models import User
from rest_framework import generics
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from api.services.product_service import ProductService


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'users': reverse('user-list', request=request, format=format),
        'customer': reverse('customer-list', request=request, format=format),
        'product': reverse('product-list', request=request, format=format),
    })


class CustomerList(generics.ListCreateAPIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)



class CustomerDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                        IsOwnerOrReadOnly]

    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer



class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ProductList(generics.ListCreateAPIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def post(self, request, *args, **kwargs):
        productService = ProductService()
        status, body = productService.get_products_by_id(self.request.data["product_fav_id"])

        if status == 404:
            return Response(data="Product not found", status=status, exception=Exception())
        elif status != 200:
            return Response(data="Oops, an error!", status=status, exception=Exception())

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer=serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=201, headers=headers)

    def perform_create(self, serializer):
        find_customer = Customer.objects.get(pk=self.request.data["customer_id"])
        serializer.save(customer=find_customer)


class ProductDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                        IsOwnerOrReadOnly]

    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
