from rest_framework import serializers
from api.models import Customer, Product
from django.contrib.auth.models import User


class CustomerSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.username')
    products = serializers.PrimaryKeyRelatedField(many=True, queryset=Product.objects.all())

    class Meta:
        model = Customer
        fields = ['id', 'name', 'email', 'user', 'products']


class UserSerializer(serializers.ModelSerializer):
    customers = serializers.PrimaryKeyRelatedField(many=True, queryset=Customer.objects.all())

    class Meta:
        model = User
        fields = ['id', 'username', 'customers']


class ProductSerializer(serializers.ModelSerializer):
    customer = serializers.ReadOnlyField(source='customer.id')

    class Meta:
        model = Product
        fields = ['id', 'customer', 'product_fav_id']
