import requests

class ProductService():
    url="http://challenge-api.luizalabs.com/api/product/{}"

    def get_products_by_id(self, product_id):
        response = requests.get(self.url.format(product_id))

        if response.status_code != 200:
            return response.status_code, None
        
        return response.status_code, response.json()
